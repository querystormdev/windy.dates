using Microsoft.Office.Interop.Excel;
using QueryStorm.Data;
using QueryStorm.Apps;
using System;
using Unity;
using Unity.Lifetime;
using static QueryStorm.Tools.DebugHelpers;
using QueryStorm.Apps.Contract;

namespace Windy_Dates
{
	public class App : ApplicationModule
	{
		public App(IAppHost host)
			:base(host)
		{
			// Register services here to make them available to components via DI, for example:
            // container.RegisterInstance(new MyServiceAbc())
		}
	}
}