using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static System.Diagnostics.Debugger;

namespace Windy.Dates
{
	public class DateFunctions
	{
		Random r = new Random();
		
		[ExcelFunction(Name = "Windy.AddSeconds")]
		public static DateTime AddSeconds(DateTime dateTime, double secondsToAdd)
			=> dateTime.AddSeconds(secondsToAdd);
		
		[ExcelFunction(Name = "Windy.AddMinutes")]
		public static DateTime AddMinutes(DateTime dateTime, double minutesToAdd)
			=> dateTime.AddMinutes(minutesToAdd);
		
		[ExcelFunction(Name = "Windy.AddDays")]
		public static DateTime AddDays(DateTime dateTime, double daysToAdd)
			=> dateTime.AddDays(daysToAdd);
			
		[ExcelFunction(Name = "Windy.AddMonths")]
		public static DateTime AddMonths(DateTime dateTime, int monthsToAdd)
			=> dateTime.AddMonths(monthsToAdd);
			
		[ExcelFunction(Name = "Windy.AddYears")]
		public static DateTime AddYears(DateTime dateTime, int yearsToAdd)
			=> dateTime.AddMonths(yearsToAdd);
	
		[ExcelFunction(Name = "Windy.FormatDate")]
		public static string FormatDate(DateTime date, string format)
			=> date.ToString(format);
			
		[ExcelFunction(Name = "Windy.RandomDate")]
		public DateTime RandomDate(DateTime startDate, DateTime endDate)
		{
			TimeSpan timeSpan = endDate - startDate;
			return startDate.AddDays(r.Next(0, (int)timeSpan.TotalDays));
		}
	}
}
